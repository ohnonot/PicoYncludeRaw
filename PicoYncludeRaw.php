<?php
/**
 * @package Pico
 * @subpackage PicoYncludeRaw
 * @author {notabug,framagit}.org/ohnonot
*/
class PicoYncludeRaw extends AbstractPicoPlugin {

    const API_VERSION = 3;
    private $include_before = '';
    private $include_after = '';
    
    public function onMetaParsed(array  $meta)
    {
        if(isset($meta['include_before'])) { $this->include_before = $meta['include_before']; }
        if(isset($meta['include_after'])) { $this->include_after = $meta['include_after']; }
    }
    public function onContentParsed(&$content)
    {
        if($this->include_before != "" and substr($this->include_before,0,1) == '/') {
            $file=$_SERVER['DOCUMENT_ROOT'].$this->include_before;
            if(file_exists($file)) $content=file_get_contents($file).$content;
        }
        if($this->include_after != "" and substr($this->include_after,0,1) == '/') {
            $file=$_SERVER['DOCUMENT_ROOT'].$this->include_after;
            if(file_exists($file)) $content=$content.file_get_contents($file);
        }
    }
}
