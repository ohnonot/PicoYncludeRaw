## A PicoCMS plugin to add raw files to page content

You can add `include_before: /path/to/file` and/or `include_after: /path/to/file` to a page's YAML header. This will insert the plain, unprocessed contents of `file` (usually some sort of HTML fragment) before or after the page's content.

## But Twig already has such an include function

...and it does exactly what is desired here, but it can only include files under the theme directory (or under the base template's path,
to be precise. At least on my system. If I'm wrong please open an issue and tell me.

### If it seems that part of your raw file disappears

Behind the scenes, most content modifying plugins use PHP's [DOMDocument class][dd] which, to sum up hours of research, is quirky.

This plugin is supposed to inject raw text - usually HTML - after all other such plugins, That's why it's called "Pico**Y**nclude" with a Y.
Because although Pico now has plugin depndencies, I didn't know how to express the above as such, and plugin naming apparently still matters
for execution order.

In other words, make sure it runs last, otherwise another plugin might go over the content (which now includes the injected file) again and
remove something by the power of DOMDocument's weird opinionatedness.

[dd]: https://www.php.net/manual/en/class.domdocument.php
